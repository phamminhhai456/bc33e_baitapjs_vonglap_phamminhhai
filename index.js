function domId(id) {
  return document.getElementById(id);
}
// Bài 1
function timSNDNN() {
  var sum = 0;
  var i = 0;
  while (sum < 10000) {
    i++;
    sum += i;
  }

  domId("ketQuaBai1").innerHTML = `Số nguyên dương nhỏ nhất : ${i}`;
}

// Bài 2
function tinhTong() {
  var x = domId("x").value * 1;
  var n = domId("n").value * 1;

  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  domId("ketQuaBai2").innerHTML = `Tổng : ${sum}`;
}

// Bài 3
function tinhGiaiThua() {
  var num = domId("num").value * 1;

  var tongGiaiThua = 1;
  for (var i = 1; i <= num; i++) {
    tongGiaiThua *= i;
  }
  domId("ketQuaBai3").innerHTML = `Giai thừa : ${tongGiaiThua}`;
}

// Bài 4

function taoThe() {
  var content = domId("ketQuaBai4");

  for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      content.innerHTML += `<div class="p-3 bg-danger text-white">Div chẳn</div>`;
    } else {
      content.innerHTML += `<div class="p-3 bg-primary text-white">Div lẻ</div>`;
    }
  }
}
